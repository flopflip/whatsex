whatsex can be found here

Installation Instructions:

1. Download the whatsex_X.tar.gz file
2. From within R, type install.packages(path_to_whatsex_file, repos = NULL, type="source")


Note: installation was tested on Ubuntu Linux, OS X (El Cap+) and Windows7,10.  

Some windows 10 users may experience problems with the file auto-opening with Winzip - 
please download and install without unzipping.

Windows 10 automatic updates are known to sporadically break different/arbitrary softwares:

As a free workaround for Windows 10 users, the installation was also tested in a free virtualbox Virtual machine with ubuntu linux:
install virtualbox from virtualbox.org, then install Lubuntu on a virtual machine, then install whatsex as described above.